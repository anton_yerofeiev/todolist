import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import Base from '../../components/Base/Base';
import withLocalStorage from '../../hoc/withLocalStorage';
import withTest from '../../hoc/withTest';
import { compose } from 'recompose';
import withRedux from "../../hoc/withRedux/withRedux";
import {connect} from "react-redux";

class App extends Component {
    constructor(props) {
        super(props);
        this.keys = {
            ENTER: 13,
            ESCAPE: 27,
        };
        this.storageName = 'AppState';



        this.state =  this.props.getFromLocalStorage(this.storageName)
                ? this.props.getFromLocalStorage(this.storageName)
                : list;

    }

    onSubmitHandler = (event) => {
        event.preventDefault();
        const data = new FormData(event.target);

        if (data.get('text').trim().length) {
            this.addItem(data.get('text'));
        }
        event.target.reset();
    };

    addItem = (text) => {
        const item = {
            text: text,
            id: Date.now(),
            isDone: false
        };
        this.setState({
            list: [...this.state.list, item]
        });
    };

    removeItem = (event, id) => {
        event.stopPropagation();

        this.setState({
            list: this.state.list.filter((item) => {
                if (item.id !== id) {
                    return item;
                }
            })
        });
    };

    modifyItem = (id, input) => {

        this.setState({
            list: this.state.list.map((item) => {
                if (item.id === id) {
                    item.isModifying = false;
                    item.text = input.value.trim();
                }
                return item;
            })
        });
    };

    cancelModifying = (event, id) => {
        event.stopPropagation();
        this.setState({
            list: this.state.list.map((item) => {
                if (item.id === id) {
                    item.isModifying = false;
                }
                return item;
            })
        });
    };

    toggleIsDone = (id) => {

        this.setState({
            list: this.state.list.map((item) => {
                if (item.id === id && !item.isModifying) {
                    item.isDone = !item.isDone;
                }
                return item;
            })
        });
    };

    onDoubleClickItemHandler = (id) => {

        this.setState({
            list: this.state.list.map((item) => {
                if (item.id === id) {
                    item.isModifying = true;
                }
                return item;
            })
        });
    };

    filterItems = (filter) => {
        this.setState({
            show: filter
        })
    };

    onKeyUpHandler = (event, id, input) => {
        if (event.keyCode === this.keys.ENTER) {
            this.modifyItem(id, input)
        } else if (event.keyCode === this.keys.ESCAPE) {
            this.cancelModifying(event, id);
        } else return false;
    };

    componentDidMount() {
        this.props.test('App did mount')
    }

    render() {
        this.props.addToLocalStorage(this.state, this.storageName);
        this.props.test('App render');

        const { dispatch, visibleTodos, visibilityFilter } = this.props;

        return (
            <Base
                list={this.state.list}
                show={this.state.show}
                onSubmitHandler={this.onSubmitHandler}
                removeItem={this.removeItem}
                onClickItem={this.toggleIsDone}
                onDoubleClickItem={this.onDoubleClickItemHandler}
                modifyItem={this.modifyItem}
                cancelModifying={this.cancelModifying}
                onKeyUpHandler={this.onKeyUpHandler}
                filterItems={this.filterItems}
            />
        );
    }
}

const hoc = compose(
    withLocalStorage,
    withTest,
    connect()
);

export default hoc(App);

const list = {
    list: [
        {
            id: 1,
            text: 'text1',
            isDone: false
        }
    ],
    show: 'all'
};