import React, {Component} from 'react';

import TodoList from '../../components/TodoList/TodoList'
import TodoForm from "../TodoForm/TodoForm";
import Filter from "../Filter/Filter";

export default class Base extends Component {


    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col">
                        <TodoList {...this.props}/>
                    </div>
                    <div className="col">
                        <TodoForm onSubmitHandler={this.props.onSubmitHandler}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <Filter {...this.props}/>
                    </div>
                </div>
            </div>
        )
    }
}
