import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './styles.css';

export default class TodoItem extends Component {

    render() {
        const isDoneClass = this.props.isDone ? 'list-group-item-light isDone' : '';
        return (
            <li
                className={`list-group-item list-group-item-primary ${isDoneClass}`}

                onDoubleClick={() => {
                    this.props.onDoubleClickItem(this.props.id)
                }}
            >
                {!this.props.isModifying ?
                    <div>
                        <span
                            className="text"
                            onClick={() => {
                                this.props.onClickItem(this.props.id)
                            }}
                        >{this.props.text}</span>
                        <button
                            className="btn btn-danger float-right"
                            onClick={(event) => {
                                this.props.removeItem(event, this.props.id)
                            }}
                        >Remove
                        </button>
                    </div> :
                    <div>
                        <div className="form-group">
                            <input
                                ref={(element) => {
                                    this.textInput = element;
                                }}
                                type="text"
                                className="form-control"
                                onKeyUp={(event) => {
                                    this.props.onKeyUpHandler(event, this.props.id, this.textInput)
                                }}
                                defaultValue={this.props.text}
                                autoFocus
                            />
                        </div>
                        <button
                            className="btn btn-primary"
                            onClick={() => {
                                this.props.modifyItem(this.props.id, this.textInput)
                            }}
                        >Change
                        </button>
                        <button
                            className="btn btn-light"
                            onClick={(event) => {
                                this.props.cancelModifying(event, this.props.id)
                            }}
                        >Cancel
                        </button>
                    </div>
                }
            </li>
        );
    }
}

TodoItem.propTypes = {
    text: PropTypes.string.isRequired,
    isModifying: PropTypes.bool,
};