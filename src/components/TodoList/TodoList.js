import React, {Component} from 'react';

import TodoItem from '../../components/TodoItem/TodoItem';

export default class TodoList extends Component {


    render() {
        const list = this.props.list.filter((item) => {
            if (this.props.show === 'done' && item.isDone) {
                return item;
            } else if (this.props.show === 'active' && !item.isDone) {
                return item;
            }
            return item;
        });

        return (
            <ul className="list-group">
                {list.map((item) => {
                    return (

                        <TodoItem
                            key={item.id}
                            {...item}
                            {...this.props}
                        />
                    )

                })}
            </ul>
        )
    }
}