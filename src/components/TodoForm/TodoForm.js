import React, {Component} from 'react';

export default class TodoForm extends Component {

    render() {
        return (
            <form onSubmit={this.props.onSubmitHandler}>
                <div className="form-group">
                    <label htmlFor="form-text">Text to do</label>
                    <input id="form-text" className="form-control" type="text" name="text"/>
                </div>

                <div className="form-group">
                    <button className="btn btn-primary">Add</button>
                </div>
            </form>
        );
    }
}