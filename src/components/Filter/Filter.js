import React, {Component} from 'react';


export default class Base extends Component {


    render() {

        return (
            <ul className="list-group">
                <li className="list-group-item">
                    <button
                        className={`btn btn-primary${this.props.show === 'all' ? ' active' : ''}`}
                        onClick={() => {this.props.filterItems('all')}}>All</button>
                </li>
                <li className="list-group-item">
                    <button
                        className={`btn btn-primary${this.props.show === 'active' ? ' active' : ''}`}
                        onClick={() => {this.props.filterItems('active')}}>Active</button>
                </li>
                <li className="list-group-item">
                    <button
                        className={`btn btn-primary${this.props.show === 'done' ? ' active' : ''}`}
                        onClick={() => {this.props.filterItems('done')}}>Done</button>
                </li>
            </ul>
        )
    }
}
