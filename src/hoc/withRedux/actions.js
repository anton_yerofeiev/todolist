export const ADD_TODO = 'ADD_TODO';
export const TOGGLE_TODO = 'TOGGLE_TODO';
export const MODIFY_TODO = 'MODIFY_TODO';
export const START_MODIFY_TODO = 'START_MODIFY_TODO';
export const CANCEL_MODIFY_TODO = 'CANCEL_MODIFY_TODO';
export const REMOVE_TODO = 'REMOVE_TODO';




export function addTodo(text) {
    return {
        type: ADD_TODO,
        text
    }
}

export function toggleTodo(id) {
    return {
        type: TOGGLE_TODO,
        id
    }
}

export function removeTodo(id) {
    return {
        type: REMOVE_TODO,
        id
    }
}

export function modifyTodo(id) {
    return {
        type: MODIFY_TODO,
        id: id,
        text: text
    }
}

export function startModifyTodo(id) {
    return {
        type: START_MODIFY_TODO,
        id: id
    }
}

export function endModifyTodo(id) {
    return {
        type: CANCEL_MODIFY_TODO,
        id: id
    }
}