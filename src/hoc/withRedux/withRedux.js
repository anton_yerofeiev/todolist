import React, {Component} from 'react';
import {Provider} from 'react-redux';
import { createStore } from 'redux';

import todoApp from './reducers';

function withRedux(WrappedComponent) {
    class WithRedux extends Component {
        constructor() {
            super();

            this.store = createStore(todoApp);
        }



        render() {
            return (
                <Provider store={this.store}>
                    {() => <WrappedComponent {...this.props} />}
                </Provider>
            )
        }

    }

    WithRedux.displayName = `WithTest(${WrappedComponent.displayName || WrappedComponent.name || 'Component'})`;

    return WithRedux;
}

export default withRedux;