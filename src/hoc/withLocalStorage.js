import React, {Component} from 'react';

function withLocalStorage(WrappedComponent) {
    class WithLocalStorage extends Component {
        constructor() {
            super();

            this.localStorage = window.localStorage;
        }

        addToLocalStorage = (item, name) => {
            this.localStorage.setItem(name, JSON.stringify(item));
        };


        getFromLocalStorage = (name) => {
            return JSON.parse(this.localStorage.getItem(name));
        };



        render() {
            return (
                <WrappedComponent
                    {...this.props}
                    addToLocalStorage={this.addToLocalStorage}
                    getFromLocalStorage={this.getFromLocalStorage}
                />
            )
        }

    }

    WithLocalStorage.displayName = `WithLocalStorage(${WrappedComponent.displayName || WrappedComponent.name || 'Component'})`;

    return WithLocalStorage;
}

export default withLocalStorage;