import React, {Component} from 'react';

function withTest(WrappedComponent) {
    class WithTest extends Component {
        constructor() {
            super();

            this.localStorage = window.localStorage;
        }

        test = (msg) => {
            console.log(msg);
        };

        componentDidMount() {
            this.test('WithTest did mount')
        }

        render() {
            return (
                <WrappedComponent
                    {...this.props}
                    test={this.test}
                />
            )
        }

    }

    WithTest.displayName = `WithTest(${WrappedComponent.displayName || WrappedComponent.name || 'Component'})`;

    return WithTest;
}

export default withTest;